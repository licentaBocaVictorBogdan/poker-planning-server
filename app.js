const express = require('express');
const createError = require('http-errors');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
require('./models/db.js');
const cors = require('cors');
const path = require('path');
const user = require('./routes/users');
const room = require('./routes/room');

const app = express();

const jwtSecret = 'secretString';

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(bodyParser.json());
app.use(express.static('public'));
app.use(expressJwt({secret: jwtSecret}).unless({path: ['/', '/signup', '/login']}));

const requestOrigin = 'http://localhost:3000';
app.use(cors({origin: requestOrigin}));


app.post('/signup', user.signup);
app.post('/login', user.login, (req, res) => {
  const token = jwt.sign({username: req.body.username, userId: req.body.userId}, jwtSecret);
  res.status(200).send({token: token, username: req.body.username});
});
app.post('/changePassword', user.changePassword);

app.post('/room', room.createRoom);
app.post('/room/:id', room.joinRoom);
app.get('/rooms', room.getRooms);
app.get('/room/:id', room.validateRoom);
app.get('/votingList/:id/', room.getVotingList);
app.post('/vote/:id', room.vote);
app.get('/chat/:id', room.getArguments);
app.post('/chat/:id', room.argument);
app.post('/chat/:id/final', room.finalVote);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;