const mongoose = require('mongoose');
const dbURL = 'mongodb://127.0.0.1:27017';

mongoose.connect(dbURL);
mongoose.set('useCreateIndex', true);

mongoose.connection.on('connected', () => {
  console.log('Mongoose connected to ' + dbURL);
});

mongoose.connection.on('error', error => {
  console.log('Mongoose connection error: ' + error)
});

mongoose.connection.on('disconnected', () => {
  console.log('Mongoose disconnected');
});
