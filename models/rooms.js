const mongoose = require('mongoose');

const roomSchema = new mongoose.Schema({
  roomId: {type: String, unique: true, index: true},
  name: String,
  description: String,
  persons: Number,
  personsList: [{personId: String, personName: String, personVote: Number}],
  personsDebate: [{personId: String, personName: String, personArgument: String}],
  createdAt: String,
  createdBy: String,
  finalVote: Number
});

module.exports = mongoose.model('Room', roomSchema);
