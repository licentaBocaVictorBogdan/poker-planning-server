const uniqid = require('uniqid');
const Room = require('../models/rooms');

exports.createRoom = function (req, res) {
  const {name, persons, description} = req.body;
  const newRoom = new Room();

  newRoom.roomId = uniqid();
  newRoom.name = name;
  newRoom.persons = parseInt(persons);
  newRoom.description = description;
  newRoom.personsList = [];
  newRoom.personsDebate = [];
  newRoom.createdAt = new Date().toJSON();
  newRoom.createdBy = getUserId(req.headers.authorization);

  newRoom.save(function (err, savedRoom) {
    if (err) {
      res.status(400).send('Create room error');
    } else {
      res.status(200).send({"roomId": savedRoom.roomId});
    }
  })
};

exports.joinRoom = function (req, res) {
  Room.findOne({roomId: req.params.id}, function (err, dbRoom) {
    if (dbRoom === null || dbRoom === undefined || dbRoom === {}) {
      res.status(404).end('Wrong room id');
    } else {
      if (dbRoom.personsList.length === dbRoom.persons) {
        res.status(404).end('Room is full');
      } else {
        const userId = getUserId(req.headers.authorization);
        if (dbRoom.personsList.filter(user => user.personId === userId).length === 0) {
          Room.update({_id: dbRoom._id}, {
            personsList: [...dbRoom.personsList, {
              personId: userId,
              personName: getUserName(req.headers.authorization)
            }]
          })
            .then(() => res.status(200).send())
        } else {
          res.status(200).send();
        }
      }
    }
  })
};

exports.vote = function (req, res) {
  Room.findOne({roomId: req.body.roomId}, function (err, room) {
    if (room === null || room === undefined || room === {}) {
      res.status(500).end('Wrong room id');
    } else {
      const {vote} = req.body;
      const index = room.personsList.map(person => person.personId).indexOf(getUserId(req.headers.authorization));
      if (index < 0) {
        res.status(404).end('Wrong person id');
      } else {
        const newPersonsList = [...room.personsList];
        newPersonsList[index].personVote = vote;
        if (newPersonsList.filter(person => person.personVote !== undefined).length === room.persons) {
          const min = newPersonsList.sort((a, b) => a.personVote > b.personVote)[0];
          const max = newPersonsList.sort((a, b) => a.personVote < b.personVote)[0];

          delete min._id;
          delete min.personVote;
          delete max._id;
          delete max.personVote;

          Room.update({_id: room._id}, {personsList: newPersonsList, personsDebate: [min, max]})
            .then(() => res.status(200).send())
        } else {
          Room.update({_id: room._id}, {personsList: newPersonsList})
            .then(() => res.status(200).send())
        }
      }
    }
  })
};

exports.validateRoom = function (req, res) {
  Room.findOne({roomId: req.params.id}, function (err, room) {
    if (room === null || room === undefined || room === {}) {
      res.status(404).end('Wrong room id');
    } else {
      res.status(200).send(room);
    }
  })
};

exports.getVotingList = function (req, res) {
  Room.findOne({roomId: req.params.id}, function (err, room) {
    if (room === null || room === undefined || room === {}) {
      res.status(404).end('Wrong room id');
    } else {
      res.status(200).send({
        personsList: room.personsList,
        getPersons: room.personsList.filter(person => person.personVote !== undefined).length !== room.persons
      });
    }
  })
};

exports.argument = function (req, res) {
  Room.findOne({roomId: req.params.id}, function (err, room) {
    if (room === null || room === undefined || room === {}) {
      res.status(404).end('Wrong room id');
    } else {
      const argument = [...room.personsDebate];
      const argumentsIds = room.personsDebate.map(person => person.personId);
      argument[argumentsIds.indexOf(getUserId(req.headers.authorization))].personArgument = req.body.argument;

      Room.update({_id: room._id}, {personsDebate: argument})
        .then(() => res.status(200).send())
    }
  })
};

exports.getArguments = function (req, res) {
  Room.findOne({roomId: req.params.id}, function (err, room) {
    if (room === null || room === undefined || room === {}) {
      res.status(404).end('Wrong room id');
    } else {
      res.status(200).send({
        personsDebate: room.personsDebate,
        prediction: room.personsList.map(pers => pers.personVote).reduce((acc, val) => val += acc, 0) / room.persons,
        finalVote: room.finalVote
      })
    }
  })
};

exports.finalVote = function (req, res) {
  Room.findOne({roomId: req.params.id}, function (err, room) {
    if (room === null || room === undefined || room === {}) {
      res.status(404).end('Wrong room id');
    } else {
      Room.update({_id: room._id}, {finalVote: req.body.finalVote})
        .then(() => res.status(200).send());
    }
  })
};

exports.getRooms = function (req, res) {
  Room.find({createdBy: getUserId(req.headers.authorization)}, function (err, rooms) {
    if (rooms === null || rooms === undefined || rooms === {}) {
      res.status(500).send('Server error')
    } else {
      res.status(200).send(rooms.reverse());
    }
  })
};

function getUserId(string) {
  return JSON.parse(new Buffer(string.split('.')[1], 'base64')).userId;
}

function getUserName(string) {
  return JSON.parse(new Buffer(string.split('.')[1], 'base64')).username;
}