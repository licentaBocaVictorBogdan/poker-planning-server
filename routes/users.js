const User = require('../models/users');
const bcrypt = require('bcryptjs');
const SALT_WORK_FACTOR = 10;

exports.signup = function (req, res) {
  const newUser = new User();
  newUser.username = req.body.username;
  newUser.email = req.body.email;
  newUser.password = req.body.password;
  newUser.companyName = req.body.companyName;

  newUser.save((err, savedUser) => {
    if (err) {
      res.status(400).send('An account with same username or email already exist');
    } else {
      res.status(200).send({"username": savedUser.username});
    }
  })
};

exports.changePassword = function (req, res) {
  User.findOne({_id: getUserId(req.headers.authorization)}, function (err, dbUser) {
    if (err) {
      res.status(500).send('Backend unexpected error');
    } else {
      dbUser.comparePassword(req.body.currentPassword, (err, isMatch) => {
        if(isMatch && isMatch === true) {
          bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
            if (err) return next(err);
            bcrypt.hash(req.body.newPassword, salt, (err, hash) => {
              if (err) return next(err);

              User.update({_id: getUserId(req.headers.authorization)}, {password: hash})
                .then(() => res.status(200).send('User password changed'))
            })
          });
        }else {
          res.status(402).send('Wrong current password');
        }
      })
    }
  })
};

exports.login = function (req, res, next) {
  const {username, password} = req.body;

  User.findOne({username: username}, function (err, user) {
    if (user === null || user === undefined || user === {}) {
      res.status(400).end('No account with this email');
    } else {
      req.body.username = user.username;
      req.body.userId = user._id;

      user.comparePassword(password, (err, isMatch) => {
        if (isMatch && isMatch === true) next();
        res.status(400).end('Invalid username or password');
      })
    }
  })
};

function getUserId(string) {
  return JSON.parse(new Buffer(string.split('.')[1], 'base64')).userId;
}